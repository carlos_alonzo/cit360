import java.util.*;
import java.io.*;

public class validations {
    public static void main(String[] args) throws InputMismatchException{
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter the first number:");
        int numb1 = input.nextInt();


        System.out.println("Please enter the second number:");
        int numb2 = input.nextInt();
        while (numb2 == 0) {
            try {
                //numb2 = input.nextInt();
                System.out.println("'0' is an invalid number.\n Please enter a number again:");
                numb2 = input.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("'0' is an invalid number");
            }

        }


        System.out.println("The result of the division between "+numb1+" and "+numb2+" is: "+ division(numb1,numb2));
    }

    public static int division(int x, int y){
        //while (y == 0){
        // System.out.println("number 2 is equal to 0");
        //}
        int result = 0;
        try{
            result = x / y;
        }
        catch(ArithmeticException e){
            System.out.println("'0' is an invalid number. Please enter a number again:");
        }
        return (result);
    }
}
