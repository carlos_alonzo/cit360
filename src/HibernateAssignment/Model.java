package HibernateAssignment;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class Model {

    SessionFactory factory = null;
    Session session = null;

    private static Model single_instance = null;

    private Model()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static Model getInstance()
    {
        if (single_instance == null) {
            single_instance = new Model();
        }

        return single_instance;
    }

    /** Used to get more than one customer from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<Student> getStudent() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from HibernateAssignment.Student";
            List<Student> cs = (List<Student>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single customer from database */
    public Student getStudent(int id_student) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from HibernateAssignment.Student where id_student=" + Integer.toString(id_student);
            Student c = (Student)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    //Code for add/insert data into the database
    public Student setStudent(Student st){
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            session.save(st);
            session.getTransaction().commit();
            return st;
        }catch (Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }

    }
}
