package HibernateAssignment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class RunHibernate {
    public static void main(String[] args) {
        Model model = Model.getInstance();
        Helper helper = new Helper();

        List<Student> c = model.getStudent();
        for(Student s : c){
            System.out.println(s);
        }
        System.out.println(model.getStudent(1));


        String newName = helper.getUserInput("Enter your Name:");
        String newLastName = helper.getUserInput("Enter your Lastname:");
        int newAge = Integer.parseInt(helper.getUserInput("Enter your age:"));
        String newLocation = helper.getUserInput("Enter your place:");
        String newEmail = helper.getUserInput("Enter your email:");
        String newGender = helper.getUserInput("Enter your gender(M/F):");

        Student st = new Student();
        st.setName(newName);
        st.setLastName(newLastName);
        st.setAge(newAge);
        st.setEmail(newEmail);
        st.setGender(newGender);
        st.setLocation(newLocation);
        System.out.println(model.setStudent(st));
    }
}

class Helper {
    public String getUserInput(String prompt){
        String inputLine = null;
        System.out.print(prompt + " ");
        try{
            BufferedReader bufferedReader = new BufferedReader(
            new InputStreamReader(System.in));
            inputLine = bufferedReader.readLine();
            if (inputLine.length() == 0) return null;
        }catch (IOException e){
            System.out.println("IOException: "+e);
        }
        return inputLine;
    }
}