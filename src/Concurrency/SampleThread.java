package Concurrency;

public class SampleThread extends Thread{
    SampleThread(){
        //Create a new second thread
        super("Demo Thread");
        System.out.println("Child Thread: "+ this);
    }

    //This is the entry point for the second thread.
    public void run(){
        try {
            for(int i = 5; i > 0; i-- ){
                System.out.println("Child Thread: "+i);
                Thread.sleep(500);
            }
        }catch (InterruptedException ie){
            System.out.println("Child Interrupted.");
        }
        System.out.println("Exiting child thread.");
    }
}

class extendThread {
    public static void main(String[] args){
        SampleThread newThread2 = new SampleThread();
        newThread2.start();

        try{
            for(int i = 5; i > 0; i--){
                System.out.println("Main Thread: "+i);
                Thread.sleep(1000);
            }
        }catch (InterruptedException ie){
            System.out.println("Main thread interrupted.");
        }
        System.out.println("Main thread exiting.");
    }
}
