package com.company;
import java.util.*;
import java.io.*;

class PriorityQ{
    private int maxSize;
    private long[] queArray;
    private int nItems;

    public PriorityQ(int s){//Constructor
        maxSize = s;
        queArray = new long[maxSize];
        nItems = 0;
    }
    public void insert(long item){
        int j;

        if (nItems==0) //if no items
            queArray[nItems++] = item; //insert 0
        else{  //if items
            for (j = nItems-1; j > 0; j--){ //start at end
                if (item > queArray[j])  //if new item larger
                    queArray[j+1] = queArray[j]; //shift upward
                else
                    break;   //done shifting
            } //end for
            queArray[j+1] = item;
            nItems++;
        } //end else
    }// end insert
    public long remove(){ //remove minimum item
        return queArray[--nItems];
    }
    public long peekMin(){ //peek at minimum item
        return queArray[nItems-1];
    }
    public boolean isEmpty() { //true is queue is empty
        return (nItems == 0);
    }
    public boolean isFull(){ //true is queue is full
        return (nItems == maxSize);
    }
} //end class PriorityQ

public class Main {
    ArrayList<Song> songList = new ArrayList<Song>(); //We will store the song titles in an ArrayList of strings

    public static void main(String[] args) throws IOException {
        System.out.println("--List--");
        System.out.println("--A list of movies sorted alphabetically--");
        new Main().go();
        System.out.println("--Queue--");
        // write your code here
        PriorityQ thePq = new PriorityQ(5);
        thePq.insert(30);
        thePq.insert(50);
        thePq.insert(10);
        thePq.insert(40);
        thePq.insert(20);

        while (!thePq.isEmpty()) {
            long item = thePq.remove();

            System.out.println(item + " "); //10, 20, 30, 40, 50
        } //end while
        System.out.println("");

        System.out.println("--Maps--");
        System.out.println("--Books & Authors--");

        HashMap<String, String> books = new HashMap<String,String>();
        books.put("War and Peace","Leo Tostoy");
        books.put("Lolita","Vladimir Nabokov");
        books.put("Ulysses","James Joyce");
        books.put("Dubliners","James Joyce");
        books.put("Pale Fire ","Vladimir Nabokov");
        books.put("Emma","Jane Austen");

        for(String i : books.keySet()){
            System.out.println(i);
        }

        System.out.println("--TreeSet--");
        System.out.println("--Print only the unique ids--");
        Set<Integer> treeSet = new TreeSet<>();
        treeSet.add(1350626964);
        treeSet.add(1350626964);
        treeSet.add(1782545326);
        treeSet.add(1456862457);
        treeSet.add(1562648954);
        treeSet.add(1350626964);

        for(int element : treeSet){
            System.out.println(element + " ");

    }
}
    public void go() {
        getSongs();
        Collections.sort(songList);
        System.out.println(songList);
    }
    void getSongs(){
        try {
            File file = new File("SongListMore.txt");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line=reader.readLine()) != null){
                addSong(line);
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    void addSong(String lineToParse){
        String[] tokens = lineToParse.split("/");
        Song nextSong = new Song(tokens[0], tokens[1], tokens[2], tokens[3]);
        songList.add(nextSong);
        //songList.add(tokens[0]);
    }
}
class Song implements Comparable<Song>{
    String title;
    String artist;
    String rating;
    String bpm;

    public int compareTo(Song s){
        return title.compareTo(s.getTitle());
    }

    Song(String t,String a, String r, String b){
        title = t;
        artist = a;
        rating = r;
        bpm = b;
    }
    public  String getTitle(){
        return title;
    }
    public String getArtist(){
        return artist;
    }
    public String getRating(){
        return  rating;
    }
    public String getBpm(){
        return bpm;
    }
    public String toString(){
        return title;
    }
}