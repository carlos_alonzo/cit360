import java.io.*;
import java.util.*;


public class dataValidation {
    public static void main (String[] args) {
        Scanner input = new Scanner(System.in);
        boolean working = false;
        int number1 = 0;
        int number2 = 0;

        while(!working) {
            System.out.println("Enter a number:");
            try {
                number1 = input.nextInt();
                working = true;
            } catch (InputMismatchException e) {
                System.out.println("Entered value is not a number.");
                input.next();
            }
        }

        while(working) {
            System.out.println("Enter a second number:");
            try {
                number2 = input.nextInt();

                working = false;
            }
            catch (InputMismatchException e) {
                System.out.println("Entered value is not a number.");
                input.next();
            }
        }

        calculateInputs(number1,number2);
    }
    static void calculateInputs(int number1, int number2) {
        try {
            int resultDivision = number1 / number2;
            System.out.println("The division result from " +number1+ " and " +number2+ " is: "+resultDivision);
        }
        catch (ArithmeticException e){
            System.out.println("You should not divide a number by zero.");
        }
        catch (Exception e){
            System.out.print("Exception occurred.");
        }

    }
}
