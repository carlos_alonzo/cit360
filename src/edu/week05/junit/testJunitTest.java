package edu.week05.junit;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
class testJunitTest {

    //@org.junit.jupiter.api.Test
    @Test
    void testObject() {
        testJunit tj = new testJunit();
        tj.setTestWord("BYU");
        tj.getLength();
        assertEquals(3,tj.getnValue());
    }
    @Test
    void testIfNotNull(){
        testJunit tj = new testJunit();
        tj.setTestWord("Carlos");
        assertNotNull(tj.getTestWord());
    }

    @Test
    void testIfNull(){
        testJunit tj = new testJunit();
        tj.setTestWord(null);
        assertNull(tj.getTestWord());
    }

    @Test
    void testFalse(){
        testJunit tj = new testJunit();
        assertFalse(tj.contains_basic("Trump"));
    }

    @Test
    void testTrue(){
        testJunit tj = new testJunit();
        assertTrue(tj.contains_basic("abcd"));
    }

    @Test
    void testArrayEquals(){
        testJunit tj = new testJunit();
        String[] splitWord = new String[] {"BYU", "Idaho", "University"};
        assertArrayEquals(splitWord,tj.split_basic("BYU Idaho University"));
    }

    @Test
    void testSame(){
        testJunit tj = new testJunit();
        tj.setTestWord("4");
        tj.setnValue(4);
        assertSame(tj.getTestWord(),tj.getTestWord());
    }

    @Test
    void testNotSame(){
        testJunit tj = new testJunit();
        tj.setTestWord("4");
        tj.setnValue(4);
        assertNotSame(tj.getnValue(),tj.getTestWord());
    }
}