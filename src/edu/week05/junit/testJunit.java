package edu.week05.junit;

public class testJunit {
    private int nValue;
    private String testWord;


    public int getnValue(){
        return nValue;
    }
    public void setnValue(int nValue){
        if(nValue < 0){
            throw new IllegalArgumentException("Number must be greater or equal to 0");
        }
        this.nValue = nValue;
    }

    public String getTestWord(){
        return testWord;
    }
    public void setTestWord(String testWord){
        if (testWord == null){
            return;
        }
        this.testWord = testWord;
    }


    void getLength(){
        nValue = testWord.length();
        System.out.println("The actual lenght of "+ testWord +" is: "+ nValue);
    }
    public boolean contains_basic(String testWord) {
        boolean contWord = false;
        try {
            contWord = testWord.contains("abcd");
        }
        catch (NullPointerException e){
            System.out.println("Cannot accepts null");
        }
        return contWord;
    }

    public String[] split_basic(String x){
        if(x == null || x.isEmpty()){
            throw new RuntimeException("Cannot be null or empty");
        }
        else {
            String splitResult[] = x.split(" ");
            return splitResult;
        }
    }
}

//class testingClass {
//    public static void main (String[] args) {
//        testJunit t = new testJunit();
//        t.setTestWord("Carlos");
//        t.getLength();
//        System.out.println("It is the second output : "+t.getnValue());
//    }
//}