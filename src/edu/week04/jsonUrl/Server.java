package edu.week04.jsonUrl;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.*;
import java.io.*;
import java.net.*;



public class Server {
    public static void main(String[] args) throws IOException{
//        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress("localhost", 8001), 0);

    server.createContext("/test",new MyHttpHandler());
    server.setExecutor(null);
    server.start();
        }
        catch (Exception e){
            System.err.print(e.toString());
        }

            URL url = new URL("http://localhost:8001/test");
            Scanner sc = new Scanner(url.openStream());
            StringBuffer stringBuffer = new StringBuffer();
            while (sc.hasNext()){
                stringBuffer.append(sc.next());
            }
            String result2 = stringBuffer.toString();
            System.out.println(result2);

           ClientAccount cAccount = Client.getHttpContent(result2);
           System.out.println(cAccount);


    }
    static class MyHttpHandler implements HttpHandler{
@Override
        public void handle(HttpExchange t) throws IOException {
    ClientAccount clientAccount = new ClientAccount();
    clientAccount.setID(1);
    clientAccount.setCurrentBalance(10);
    clientAccount.setLastname("Alonzo");
    clientAccount.setName("Carlos");
    ObjectMapper objectMapper =  new ObjectMapper();
    String s = objectMapper.writeValueAsString(clientAccount);

    String response = s;
    t.sendResponseHeaders(200, response.length());
    OutputStream os = t.getResponseBody();
    os.write(response.getBytes());
    //os.close();

        }

    }


}
