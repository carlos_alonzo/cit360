package edu.week04.jsonUrl;

public class ClientAccount {
    private int ID;
    private String Name;
    private String Lastname;
    private int CurrentBalance;

    public int getID() {
        return ID;
    }
    public void setID(int ID) {
        this.ID = ID;
    }
    public String getName(){
        return Name;
    }
    public void setName(String Name){
        this.Name = Name;
    }
    public String getLastname(){
        return Lastname;
    }
    public void setLastname(String Lastname){
        this.Lastname = Lastname;
    }
    public int getCurrentBalance(){
        return CurrentBalance;
    }
    public void setCurrentBalance(int currentBalance){
        this.CurrentBalance = currentBalance;
    }

    public String toString(){
        return "Id: "+ ID + " Name: " + Name + " Lastname: " + Lastname + " Current Balance: " + CurrentBalance;
    }
}
