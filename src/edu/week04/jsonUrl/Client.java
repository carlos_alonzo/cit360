package edu.week04.jsonUrl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;

import java.io.*;
import java.net.*;
public class Client {

    public static  ClientAccount getHttpContent(String s) throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        ClientAccount accountCl = null;

        accountCl = mapper.readValue(s, ClientAccount.class);
        return accountCl;
    }
}
