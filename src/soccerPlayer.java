import java.util.*;
import java.io.*;


public class soccerPlayer {
    private String lastName;
    private int age;
    private String playingPosition;

    public String getLastName(){
        return lastName;
    }
    public void setLastName(String lastName){
        this.lastName = lastName;
    }
    public int getAge(){
        return age;
    }
    public void setAge(int age){
        this.age = age;
    }
    public String getPlayingPosition(){
        return playingPosition;
    }
    public void setPlayingPosition(String playingPosition){
        this.playingPosition = playingPosition;
    }
    public String toString(){
        return "Last Name: "+lastName+" Age: "+age+" Playing position: "+playingPosition;
    }

}
