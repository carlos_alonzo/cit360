import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpExchange;

import java.net.*;

import java.io.*;


public class HTTPServerJava {

    public static String soccerPlayerToJSON(soccerPlayer soccerPlayer){
        ObjectMapper mapper = new ObjectMapper();

        String s ="";
        try {
            s = mapper.writeValueAsString(soccerPlayer);
        } catch (JsonProcessingException e){
            System.err.println(e.toString());
        }

        return s;
    }
    public static soccerPlayer JSONToSoccerPlayer(String s){
        ObjectMapper mapper = new ObjectMapper();
        soccerPlayer player = null;

        try{
            player = mapper.readValue(s, soccerPlayer.class);
        }catch (JsonProcessingException e){
            System.err.println(e.toString());
        }
        return player;
    }

    public static void main(String[] args) throws Exception{

        HttpServer server = HttpServer.create(new InetSocketAddress(8000),0);
        server.createContext("/test", new MyHandler());
        server.setExecutor(null); //create a default executor.
        server.start();

        //Client: Call the server that I created to get the JSON data and convert that data into a Java Object
        //soccerPlayer sendPlayer =

//        soccerPlayer playerStatus = new soccerPlayer();
//        playerStatus.setLastName("Mbapee");
//        playerStatus.setAge(21);
//        playerStatus.setPlayingPosition("Midfielder");
//
//        String json = HTTPServerJava.soccerPlayerToJSON(playerStatus);
//        System.out.println(json);
//
//        soccerPlayer player2 = HTTPServerJava.JSONToSoccerPlayer(json);
//        System.out.println(player2);


    }
    static class MyHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException{

            soccerPlayer playerStatus = new soccerPlayer();
            playerStatus.setLastName("Mbapee");
            playerStatus.setAge(21);
            playerStatus.setPlayingPosition("Midfielder");

            String json = HTTPServerJava.soccerPlayerToJSON(playerStatus);
            System.out.println(json);

            soccerPlayer player2 = HTTPServerJava.JSONToSoccerPlayer(json);
            System.out.println(player2);

            //String response = "Starting the server";
            String response = json;
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();



        }
    }
}
