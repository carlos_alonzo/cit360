import java.util.InputMismatchException;
import java.util.Scanner;

public class dataValidation2 {
    public static void main (String[] args) {
        Scanner input = new Scanner(System.in);
        boolean working = false;
        double number1 = 0;
        double number2 = 0;

        while(!working) {
            System.out.println("Enter a number:");
            try {
                number1 = input.nextInt();
                working = true;
            } catch (InputMismatchException e) {
                System.out.println("Entered value is not a number.");
                input.next();
            }
        }
        while(working) {
            System.out.println("Enter a second number:");
            try {
                number2 = input.nextInt();
                if (number2==0){
                    System.out.println("You should not divide a number by cero.");
                    //input.next();
                }else {
                    working = false;
                }
            }
            catch (InputMismatchException e) {
                System.out.println("Entered value is not a number.");
                input.next();
            }
            catch (ArithmeticException e){
                System.out.println(e+" is not a number valid for a division");
            }
        }
        calculateInputs(number1,number2);
    }
    static void calculateInputs(double number1, double number2) {
        Scanner scan = new Scanner(System.in);
        try {
            double resultDivision = number1 / number2;
            System.out.println("The division result from " +number1+ " and " +number2+ " is: "+resultDivision);
        }
        catch (ArithmeticException e){
            System.out.println("You should not divide a number by zero.");
        }
        catch (Exception e){
            System.out.print("Exception occurred.");
        }

    }
}
