<%--
  Created by IntelliJ IDEA.
  User: Usuario
  Date: 4/7/2021
  Time: 9:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sign Up | Trump's College</title>
    <link rel="stylesheet" href="style.css" media="screen">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
</head>
<body>
<header>
    <img src="img/trump3.png" alt="Trump">
    <h1>Trumps' College</h1>
    <a href="ServletProcess?action=newAccount"><span class="material-icons" id="userLogo">account_circle</span></a>
</header>
<main>
    <h2>Sign Up</h2>
    <form action="ServletProcess" method="POST">
        <fieldset class="form_st">
            <label>Name</label>
            <input type="text" name="name">
            <label>Last Name</label>
            <input type="text" name="lastName" >
            <label>Email address</label>
            <input type="email" name="email" >
            <label>Password</label>
            <input type="password" name="passwd">
            <input type="submit" name="submit" value="Continue to Registration">
            <input type="hidden" name="action" value="register">
        </fieldset>

    </form>
</main>
<footer>

</footer>
</body>
</html>
