package main.java.serverSide;

import java.io.*;
import java.util.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;


@WebServlet(name = "ServletProcess", value = "/ServletProcess")
public class ServletProcess extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException{
        System.out.println("Get");

        String url = "/index.jsp";

        String action = request.getParameter("action");
        if(action == null){
            action = "return";
        }


        if(action.equals("return")){
            url = "/index.jsp";
        }
        else if(action.equals("newAccount")){
            url = "/sign-up.jsp";
        }

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }



    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response)
            throws ServletException, IOException {
        Student stud = new Student();
        Model model = Model.getInstance();


        String url = "/sign-up.jsp";

        //get current action
        String action = request.getParameter("action");
        if(action == null){
            action = "return";
        }

        if(action.equals("return")){
            url = "/sign-up.jsp";
        }
        else if(action.equals("register")){
            String firstName = request.getParameter("name");
            String lastName = request.getParameter("lastName");
            String email = request.getParameter("email");
//            boolean verifiedEmail = isValidEmailAddress(email);
            String password = request.getParameter("passwd");





            newStudent NewStudent = new newStudent(firstName,lastName,email,password);

            String message;
            if(firstName == null || lastName == null || email == null ||
                    password == null || firstName.isEmpty() || lastName.isEmpty() ||
                    email.isEmpty() || password.isEmpty()){
                message = "Please fill out all boxes.";
                url = "/sign-up.jsp";
            }else {
                message="";
                stud.setName(firstName);
                stud.setLastName(lastName);
                stud.setEmail(email);
                stud.setPassword(password);
                System.out.println(model.setStudent(stud));
                url = "/portal.jsp";
            }



            request.setAttribute("NewStudent", NewStudent);
            request.setAttribute("message",message);
//            url = "/portal.jsp";

        }
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request,response);
    }

}
