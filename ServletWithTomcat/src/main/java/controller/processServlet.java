package controller;
import java.io.*;
import java.util.*;
import java.net.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;


@WebServlet(name = "processServlet", value = "/processServlet")
public class processServlet extends HttpServlet {
//    @Override
//    protected void doGet(HttpServletRequest request,
//                         HttpServletResponse response)
//        throws ServletException, IOException{
//        System.out.println("Get");
//    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response)
        throws ServletException, IOException {

//        PrintWriter writer = response.getWriter();
//        writer.println("<html><body>");
//        writer.println("<p>Hello banda!</p>");
//        writer.println("</body></html>");


        String url = "/index.jsp";

        //Get current action
        String action = request.getParameter("action");
        if(action == null){
            action = "return"; //default action
        }

        //Perform action
        if(action.equals("return")){
            url = "/index.jsp";
        }
        else if(action.equals("register")){
            //get parameters from request
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String email = request.getParameter("userEmail");
//            boolean verifiedEmail = isValidEmailAddress(email);
            String password = request.getParameter("userPsswd");

            //Store data in User object and save User object in database
            User user = new User(firstName, lastName, email, password);
//            UserDB.insert(user);

            //Validate parameters
            String message;
            if(firstName == null || lastName == null || email == null ||
            password == null || firstName.isEmpty() || lastName.isEmpty() ||
            email.isEmpty() || password.isEmpty()){
                message = "Please fill out all boxes.";
                url = "/index.jsp";
            }else {
                message="";
                url = "/confirmation.jsp";
            }

            //set user objet in request object and ser URL
            request.setAttribute("user", user);
            request.setAttribute("message",message);
            //url = "/confirmation.jsp";

        }
        //forward request and response objects to specified URL
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }

//    public static boolean isValidEmailAddress(String email){
//        boolean result = true;
//        try{
//            InternetAddress internetAddress = new InternetAddress(email);
//            internetAddress.validate();
//        }catch (AddressException ex){
//        result = false;
//        }
//        return result;
//    }
}
