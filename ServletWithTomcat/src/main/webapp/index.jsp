<%--
  Created by IntelliJ IDEA.
  User: Usuario
  Date: 3/30/2021
  Time: 1:03 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Java Community | Sign in</title>
    <link rel="stylesheet" href="styles.css" media="screen">
</head>
<body>
<h1>Welcome to Java Community CIT-360</h1>
<h2>Java Community Signup</h2>
<p style="color:#a52422;">${message}</p>
<form action="processServlet" method="POST">
    <fieldset>
        <legend>Join our Java Community and Improve your programming skills</legend>
        <label>First Name</label>
        <input type="text" name="firstName" id="newName">
        <label>Last Name</label>
        <input type="text" name="lastName">
        <label>Email</label>
        <input type="email" name="userEmail">
        <label>Password</label>
        <input type="password" name="userPsswd">
        <input type="submit" name="submit" value="Register" id="submit">
        <input type="hidden" name="action" value="register">
    </fieldset>
</form>
</body>
</html>
