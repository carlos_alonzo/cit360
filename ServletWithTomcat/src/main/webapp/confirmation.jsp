<%--
  Created by IntelliJ IDEA.
  User: Usuario
  Date: 3/30/2021
  Time: 2:47 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Java Community | Confirmation</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<h1>Thanks for registering!</h1>
<label>Name:</label>
<p><span class="list">${user.firstName}</span></p>
<label>Last Name:</label>
<p><span class="list">${user.lastName}</span></p>
<label>Email:</label>
<p><span class="list">${user.email}</span></p>
</body>
</html>
